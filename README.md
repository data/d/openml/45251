# OpenML dataset: pathfinder_10

https://www.openml.org/d/45251

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Pathfinder Bayesian Network. Sample 10.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-verylarge.html#pathfinder)

- Number of nodes: 109

- Number of arcs: 195

- Number of parameters: 72079

- Average Markov blanket size: 3.82

- Average degree: 3.58

- Maximum in-degree: 5

**Authors**: D. Heckerman, E. Horwitz, and B. Nathwani.

**Please cite**: ([URL](https://pubmed.ncbi.nlm.nih.gov/1635470/)): D. Heckerman, E. Horwitz, and B. Nathwani. Towards Normative Expert Systems: Part I. The Pathfinder Project. Methods of Information in Medicine, 31:90-105, 1992.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45251) of an [OpenML dataset](https://www.openml.org/d/45251). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45251/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45251/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45251/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

